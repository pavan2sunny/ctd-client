import {Redirect, Route} from "react-router-dom";
import React from "react";
import {connect} from "react-redux";

const ProtectedRoute =({user:user, component: Component, ...rest }) => (
    <Route {...rest} render={(componentProps) => (
        typeof user !=='undefined' ?
            <Component {...componentProps} /> : <Redirect to={{ pathname: '/login', state: { from: componentProps.location }}} />
    )} />
);
const mapStateToProps=(state)=>{
    return{
        user:state.userDetails
    }
}
export default connect(mapStateToProps)(ProtectedRoute)