import React, {useEffect, useState} from 'react'
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import AuthenticatedAxiosInstance from "../../service/authenticatedAxiosInstance";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
}));

const AdminComponent=props=>{
    const classes = useStyles();
    useEffect(() => {
        AuthenticatedAxiosInstance.get("admin/forms/0").then(value => {
            setTdata(value.data)
            console.log(value.data)
        }).catch(reason => {
            console.log(reason.message)
        })

    },[])
    const [tdata,setTdata]=useState([] )
    return(
        <Container maxWidth={"md"}>
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Form id</TableCell>
                            <TableCell align="right">User Name</TableCell>
                            <TableCell align="right">Csv 1</TableCell>
                            <TableCell align="right">Csv 2</TableCell>
                            <TableCell align="right">Status</TableCell>
                            <TableCell align="right">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tdata.map(row => (
                            <TableRow key={row.id}>

                                <TableCell scope="row">
                                    <Typography variant={"body2"}>{row.id}</Typography>
                                </TableCell>
                                <TableCell scope="row">
                                    <Typography variant={"body2"}>{row.email}</Typography>
                                </TableCell>
                                <TableCell align="right"><a  target="_blank" href={"http://localhost:8080/file/get/"+row.fileone}>File One</a></TableCell>
                                <TableCell align="right"><a  target="_blank" href={"http://localhost:8080/file/get/"+row.filetwo}>File Two</a></TableCell>
                                <TableCell align="right"><Typography variant={"body2"}>{row.status}</Typography></TableCell>
                                <TableCell align="right"><Button onClick={event => {
                                AuthenticatedAxiosInstance.get('admin/form/'+row.id+'/approve').then(
                                    value => {
                                        alert(value.data)
                                    }
                                ).catch(
                                    reason => console.log(reason.message)
                                )
                                }}>Approve</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        </Container>
    )
}
export default AdminComponent