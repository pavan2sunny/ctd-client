import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
}));

const ScheduleTable=(props)=>{

    const classes = useStyles();
    return(
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>id</TableCell>
                        <TableCell>user</TableCell>
                        <TableCell>file-one</TableCell>
                        <TableCell>file-two</TableCell>
                        <TableCell>status</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {typeof props.data!=="undefined"&&props.data.map(row => (
                        <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                                {row.id}
                            </TableCell>
                            <TableCell align="right">{row.user}</TableCell>
                            <TableCell align="right"><a href={"http://localhost:8080/file/get/"+row.fileone} target={"_blank"}>FIleOne</a></TableCell>
                            <TableCell align="right"><a href={"http://localhost:8080/file/get/"+row.filetwo} target={"_blank"}>FIleTwo</a></TableCell>
                            <TableCell align="right">{row.status}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    )
}
export default ScheduleTable;