import React, {useEffect, useState} from 'react'
import {connect} from "react-redux";
import {Container} from "@material-ui/core";
import AuthenticatedAxiosInstance from "../service/authenticatedAxiosInstance";
import ScheduleTable from "./scheduleList/ScheduleTable";

const DashBoard=(props)=>{
    const[schedule,setSchedule]=useState({})
    let date={}
    useEffect(() => {
        console.log(props)
        console.log(props.user)
        if(typeof props.user==='undefined'||props.user===null){
            props.history.push('/login')
        }else{
            AuthenticatedAxiosInstance.post('/schedules').then((response)=>{
                date=response.data;
                console.log(response)
                alert(JSON.stringify(response.data))
            }).catch((error)=>{
                alert(error)
                console.log(error)
            })
        }

    })


    return(
    <Container maxWidth={"md"}><ScheduleTable date={date}/></Container>
    )
}
const mapStateToProps=(state)=>{
    return{
        user:state.userDetails.user
    }
}
export default connect(mapStateToProps)(DashBoard)