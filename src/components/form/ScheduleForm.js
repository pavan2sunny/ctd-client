import React, {useEffect} from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Card, TableBody } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { DatePicker, TimePicker } from '@material-ui/pickers';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TableHead from '@material-ui/core/TableHead';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import moment from 'moment'
import AuthenticatedAxiosInstance from '../../service/authenticatedAxiosInstance'
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import Chip from "@material-ui/core/Chip";
import {extendMoment} from "moment-range";
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
const useStyles = makeStyles(theme => ({
  root: {
    width: '90%',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
  container: {
    marginTop: theme.spacing(2),
  },
  heading: {

  },
  stepAction:{
    flex:1,
    display:"flex",
    alignContent:"flex-end"
  }
}));

export const Form = props => {
  const classes = useStyles(props.theme);
  const [initData,setInitData]=useState(false)
  const [formData,setFormData]=useState([{subjects:[]}])
  const [drange,setDrange]=useState([]  )
  const [activeStep, setActiveStep] = React.useState(0);
  const [values, setValues] = useState({
    //unit:{name:'',topic:[]}
    name: '',
    subject: '',
    grade: '',
    programme: '',
    from: moment(new Date()).format("YYYY/MM/DD"),
    to: moment(new Date()).format("YYYY/MM/DD"),
    fromTime: new Date(),
    toTime: new Date(),
    units: [],
    dates:[],
    holidayList:[]
  });
  const [xunits, setXuints] = useState([]);
  const [subjects,setSubjects]=useState([]);
  useEffect(()=>{
    AuthenticatedAxiosInstance.post("/formData").then(response=>{
      console.log(JSON.stringify(response.data))
      var map=new Map();
      setFormData(response.data)
      console.log(response.data)
      console.log(response.data.find((value) =>value.grade===10 ).subjects.find(value => value.name==='Physics').topicsModel)
      //setSubjects(response.data.find((value) =>value.grade===values.grade ).subjects)
      //
      setInitData(true)
      let range=moment().range(values.from,values.to);
      let diff = range.diff('date');
      //let array = range.toArray('date');

      // let diff=range.diff('date');
      console.log("tange", diff)

      // setDrange(...range.toArray('date'))
    }).catch(error=>{
      // alert(error.message)})
    // console.log("hello")
  })},[])
  // eslint-disable-next-line no-unused-vars
  const [res, setRes] = useState([]);
  const steps = 3;

  function handleNext() {
    if (steps - 1 === activeStep) {
      console.log(values)
      AuthenticatedAxiosInstance.post('/form', values)
        .then(response => {
          console.log(response);
          setRes(response.data)
          setActiveStep(prevActiveStep => prevActiveStep + 1);
        })
        .catch(error => {
          console.log(error.statusCode);
          console.log(error);
        });
    } else {
      setActiveStep(prevActiveStep => prevActiveStep + 1);
    }
    /*if (activeStep === 1) {
      console.log(values);
      AuthenticatedAxiosInstance
        .post('/topics', values)
        .then(response => {
          console.log(response.data);
          setXuints(response.data);
          console.log(xunits);
          setActiveStep(prevActiveStep => prevActiveStep + 1);
        })
    }
    */
    if (steps===activeStep){
      AuthenticatedAxiosInstance.post('/new/schedule',res).then(value => {
        if (value.statusCode===200){
          console.log(value.statusCode);
          props.history.push("/");
        }
      }).catch(reason => {
        console.log(reason)
          alert("error on back-end please try later")
      })
    }
  }

  function handleBack() {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  }

  function handleReset() {
    setActiveStep(0);
  }
  // function getStyles(theme) {
  //   return {
  //     fontWeight:theme.typography.fontWeightMedium,
  //   };
  // }


  const handleInutChange = event => {
    console.log(values);
    setValues({ ...values, [event.target.name]: event.target.value });
    console.log(event.target.name)

  };
  const isChecked = (topic, unit) => {
    return values.units.find((elements => {
      return elements.unit_name === unit && elements.topics_name.indexOf(topic) !== -1;
    }))
  }
  return (
    <Container maxWidth={"md"} className={classes.root}>
      <Card className={classes.container}>
        <Stepper activeStep={activeStep} orientation="vertical">

          <Step key={0}>
            <StepLabel>Enter Details</StepLabel>
            <StepContent>
              <div id="form">
                <TextField
                  id="outlined-simple-start-adornment"
                  variant="outlined"
                  fullWidth
                  name="name"
                  onChange={handleInutChange}
                  label="Faculty Name"
                  value={values.name}
                  InputLabelProps={{ shrink: true }}
                />
                <TextField
                  name="grade"
                  select
                  id="outlined-simple-start-adornment"
                  variant="outlined"
                  fullWidth
                  label="Grade"
                  value={values.grade}
                  InputLabelProps={{ shrink: true }}
                  margin={'normal'}
                  onChange={handleInutChange}
                >
                  {formData.map(value => (
                    <MenuItem key={value.grade} value={value.grade}>
                      {value.grade}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  select
                  name="subject"
                  id="outlined-simple-start-adornment"
                  variant="outlined"
                  fullWidth
                  label="Programme"
                  value={values.subject}
                  InputLabelProps={{ shrink: true }}
                  margin={'normal'}
                  onChange={handleInutChange}
                >

                  <MenuItem key="Bosch" value="Bosch">
                    {"BOSCH"}
                  </MenuItem>
                  <MenuItem key="Bosch" value="Capgemini">
                    {"Capgemini"}
                  </MenuItem>
                  <MenuItem key="Bosch" value="Asscendos">
                    {"Asscendos"}
                  </MenuItem>

                </TextField>
                <DatePicker
                  disableToolbar
                  // @ts-ignore
                  variant="outlined"
                  label="Form"
                  helperText="No year selection"
                  value={values.from}
                  onChange={date => {
                    setValues({ ...values, from: moment(date).format("YYYY/MM/DD") })}}
                />

                <DatePicker
                  disableToolbar
                  label="To"
                  helperText="No year selection"
                  value={values.to}
                  onChange={date => {
                    let Moment=extendMoment(moment)
                    setValues({ ...values, to: moment(date).format("YYYY/MM/DD") })
                    setDrange(()=>{
                    let range=Moment().range(values.from,moment(date).format("YYYY/MM/DD"));
                    let diff = range.diff('days');
                    console.log(diff)
                    let days=[]
                      days.push(values.from)
                    // let diff=range.diff('date');
                      for (var i=1;i<diff;i++){
                        days.push(moment(moment(days[i-1]).add(1,'days')).format("YYYY/MM/DD"))
                      }

                    return days;
                    //return range.toArray('days')
                  })
                  }
                  }

                />
                <TimePicker
                  clearable
                  ampm={true}
                  label="From TIme"
                  value={values.fromTime}
                  onChange={date => setValues({ ...values, fromTime: date })}
                />
                <TimePicker
                  clearable
                  ampm={true}
                  label="To Time"
                  value={values.toTime}
                  onChange={date => setValues({ ...values, toTime: date })}
                />
              </div>
              <FormControl disabled={values.from===values.to} className={classes.formControl}>
                <InputLabel htmlFor="select-multiple-chip">Chip</InputLabel>
                <Select
                    multiple
                    value={values.dates}
                    onChange={event => {
                      let dates=values.dates;
                      console.log(event.target.value)
                      setValues({...values,dates:event.target.value})
                    }}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={selected => (
                        <div className={classes.chips}>
                          {selected.map(value => (
                              <Chip key={value} label={value} className={classes.chip} />
                          ))}
                        </div>
                    )}
                    MenuProps={MenuProps}
                >
                  {
                  drange.map(name => (
                        <MenuItem key={name} value={name}>
                          {name}
                        </MenuItem>
                    ))
                  }
                </Select>
              </FormControl>
              <div className={classes.actionsContainer}>
                <div className={classes.stepAction}>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                      disabled={values.grade===''}
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
          <Step key={1}>
            <StepLabel>Choose Subject</StepLabel>
            <StepContent>
              <TextField
                select
                disabled={values.grade===''}
                name="subject"
                id="outlined-simple-start-adornment"
                variant="outlined"
                fullWidth
                label="Subject"
                value={values.subject}
                InputLabelProps={{ shrink: true }}
                margin={'normal'}
                onChange={handleInutChange}
              >
                {initData&&values.grade!==''&&formData.find(value => value.grade===values.grade).subjects.map(subject =>{
                  //setSubjects(subject)
                  console.log(subject)
                  //setXuints(subject.topicsModel)
                  return (<MenuItem key={subject.name} value={subject.name}>{subject.name}</MenuItem>)
                })}
              </TextField>
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={values.subject===''}
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
          <Step key={2}>
            <StepLabel>Choose Units</StepLabel>
            <StepContent>
              <Grid container spacing={3} justify={'space-evenly'}>
                {initData&&values.grade!==''&&values.subject!==''&&formData.find((value) =>value.grade===10 ).subjects.find(value => value.name==='Physics').topicsModel.map(value => (
                  <Grid xs item={true}>
                    <ExpansionPanel>
                      <ExpansionPanelSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                      >
                        <Typography className={classes.heading}>
                          {value.unit_name}
                        </Typography>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails>
                        <List>
                          {value.topics_name.map(xvalue => (

                            <ListItem key={xvalue}>
                              <FormControlLabel
                                control={
                                  <Checkbox
                                    checked={
                                      isChecked(xvalue)
                                    }
                                    onChange={() => {

                                      let indexofElement = values.units.findIndex((element => {
                                        return element.unit_name === value.unit_name;
                                      }));
                                      let tempunits = values.units;
                                      if (indexofElement === -1 || typeof indexofElement === 'undefined') {
                                        tempunits.push({ unit_name: value.unit_name, topics_name: [xvalue] })
                                        setValues({ ...values, units: tempunits })
                                      } else if (typeof values.units[indexofElement] !== 'undefined' && indexofElement !== -1 && values.units[indexofElement].topics_name.indexOf(xvalue) === -1) {
                                        tempunits[indexofElement].topics_name.push(xvalue)
                                        setValues({ ...values, units: tempunits });
                                      } else {
                                        let indexOfUnit = tempunits[indexofElement].topics_name.indexOf(xvalue)
                                        tempunits[indexofElement].topics_name.splice(indexOfUnit, 1)
                                        setValues({ ...value, units: tempunits })
                                      }
                                    }}
                                  />
                                }
                                label={xvalue}
                              />
                            </ListItem>
                          ))}
                        </List>
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  </Grid>
                ))}
              </Grid>
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>

          <Step key={3}>
            <StepLabel>Generated Schedule</StepLabel>
            <StepContent>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Subject</TableCell>
                    <TableCell>Grade</TableCell>
                    <TableCell>Unit</TableCell>
                    <TableCell>Period</TableCell>
                    <TableCell>From</TableCell>
                    <TableCell>From HH:mm</TableCell>                    
                    <TableCell>To HH:mm</TableCell>
                  </TableRow>

                </TableHead>
                <TableBody>

                  {
                    res.map(value => (
                      <TableRow key={value}>
                        <TableCell>
                          {value.subject}
                        </TableCell>
                        <TableCell>
                          {value.grade}
                        </TableCell>
                        <TableCell>
                          {value.unit}
                        </TableCell>
                        <TableCell>
                          {value.period}
                        </TableCell>
                        <TableCell>
                          {value.date}
                        </TableCell>
                        <TableCell>
                          {moment(value.startTime).format("HH:mm")}
                        </TableCell>
                        <TableCell>
                          {moment(value.endTime).format("HH:mm")}
                        </TableCell>
                      </TableRow>
                    ))}

                </TableBody>
              </Table>
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>

        </Stepper>
        {activeStep === steps.length &&
          <Paper square elevation={0} className={classes.resetContainer}>
            <Typography>All steps completed - you&apos;re finished</Typography>
            <Button onClick={handleReset} className={classes.button}>
              Reset
            </Button>
          </Paper>}
      </Card>
    </Container>
  );
};
