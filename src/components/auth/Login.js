import React, {useEffect, useState} from 'react'
import Container from "@material-ui/core/Container";
import {connect} from "react-redux";
import {Card} from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import axios from 'axios'
import {userLoggedInAction} from "../../dux/actions/actions";

const Login = (props) => {
    //InputHandlers
    useEffect(()=>{
        if (props.user.username!=null) {
            props.history.push('/')
        }
    })
    const inputChangeHandler = (event) => {
        setCredentials({...credentials, [event.target.name]: event.target.value})
    }
    const formSubmit = (event) => {
        event.preventDefault();
        axios.post('http://localhost:8080/login', credentials).then((response) => {
            if (typeof Storage !== 'undefined') {
                localStorage.setItem('user', JSON.stringify(response.data))
            }
            props.dispatch(userLoggedInAction(response.data));

            props.history.push('/')
        }).error(msg => {
            console.log(msg)
        })
    }
    const [credentials, setCredentials] = useState({
        username: '',
        password: ''
    })

    return (
        <Container maxWidth={"sm"}>
            <Card>
                <CardHeader title={"Login"}>

                </CardHeader>
                <CardContent>
                    <form id="login_form">
                        <TextField label="email" onChange={inputChangeHandler} variant={"outlined"} margin={"normal"}
                                   name="username" type="email"
                                   fullWidth={true} value={credentials.username} required={true}/>
                        <TextField label="Password" onChange={inputChangeHandler} variant={"outlined"} margin={"normal"}
                                   name="password" type="password"
                                   fullWidth={true} value={credentials.password} required={true}/>
                    </form>
                </CardContent>
                <CardActions>
                    <Button variant={"contained"} onClick={() => {
                        axios.post('http://localhost:8080/login', {
                            username: "pavan.badugucse@gmail.com",
                            password: "codesign123"
                        }).then((response) => {
                            console.log(response)
                            props.dispatch(userLoggedInAction(response.data));
                            if (typeof (Storage) !== 'undefined') {
                                localStorage.setItem('user', JSON.stringify(response.data))
                                console.log(JSON.parse(localStorage.getItem('user')))
                            }
                            props.history.push('/')
                        }).catch(msg => {
                            console.log(msg)
                        })
                    }}>Login</Button>
                </CardActions>
            </Card>

        </Container>
    )
}
const mapStateToProps = (state) => {
    return {
        user: state.userDetails
    }
}
export default connect(mapStateToProps)(Login)