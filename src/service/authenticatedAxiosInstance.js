import axios from 'axios'
const AuthenticatedAxiosInstance=axios.create({
        baseURL: 'http://localhost:8080/',
        timeout: 10000,
        headers:{
            Authorization: JSON.parse(getUserDetails())!==null?'Bearer '+JSON.parse(getUserDetails()).access_token:null
        }

});
function  getUserDetails(){
    return localStorage.getItem('user')
}
export default AuthenticatedAxiosInstance;
