import React, {useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import makeStyles from "@material-ui/core/styles/makeStyles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import {AccountCircle} from "@material-ui/icons";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {BrowserRouter, Link as RouterLink, Redirect, Route, withRouter} from "react-router-dom";
import Link from "@material-ui/core/Link";
import Login from "./components/auth/Login";
import DashBoard from "./components/DashBoard";
import {connect} from "react-redux";
import {Button} from "@material-ui/core";
import {Form} from "./components/form/ScheduleForm";
import {USER_LOGGED_IN} from "./dux/ActionTypes";
import {userLoggedInAction, userLoggedOutAction} from "./dux/actions/actions";
import ProtectedRoute from "./components/protectedRoute/ProtectedRoute";
import AdminComponent from "./components/admin/Admin";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        textAlign: 'start',

    },
    nlinks: {
        display:"flex",
        justifyContent:"flex-end",
        marginRight: theme.spacing(2)
    },
    navLink: {
        paddingRight: theme.spacing(1),

    },
    body: {
        marginTop: theme.spacing(2)
    }
}));

function App(props) {
    useEffect(() => {
        console.log(props.user)
    });
    const classes = useStyles();
    const [auth, setAuth] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    function handleChange(event) {
        setAuth(event.target.checked);
    }

    function handleMenu(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose(target) {
        setAnchorEl(null);
        props.history.replace(target)
    }

    return (
        <div className="App">
            <AppBar position="static">
                <Toolbar>
                    <Link className={classes.title} color={"inherit"} variant={"h4"} component={RouterLink} to="/"
                          underline={"none"}>Try Byte</Link>
                    <div style={{flex:1}}>
                        {typeof props.user !== 'undefined' && props.user!==null&&(
                            <div className={classes.nlinks}>
                                {props.user.roles!==undefined&&props.user.roles.indexOf("ADMIN") !== -1 &&
                                <Link style={{marginRight: "inherit"}} variant={"h6"} component={RouterLink}
                                      color={"inherit"} underline={"none"} to={"/admin"}>#Admin</Link>
                                }
                                <Link color={"inherit"} variant={"h6"} underline={"none"}
                                      style={{marginRight: "inherit"}} component={RouterLink} to="/form">Schedule</Link>
                                <Link color={"inherit"} variant={"h6"} underline={"none"}
                                      style={{marginRight: "inherit"}} component={RouterLink}
                                      to="/form">Reschedule</Link>
                                <Button variant={"outlined"} onClick={()=>{
                                    localStorage.clear()
                                    props.dispatch(userLoggedOutAction({}))
                                    props.history.push('/login')
                                }}>Logout</Button>
                            </div>
                        )}
                        {(typeof props.user === 'undefined' || props.user===null)&&(

                            <div className={classes.nlinks}>
                                <Link to="/login" variant={"h5"} component={RouterLink}
                                      color={"inherit"}
                                      underline={"none"}>Login</Link>
                            </div>
                        )}
                    </div>
                </Toolbar>

            </AppBar>
            <div className={classes.body}>
                <ProtectedRoute path="/" exact component={DashBoard}/>
                <ProtectedRoute path={"/admin"} component={AdminComponent}/>
                <ProtectedRoute path="/form" component={Form}/>
                <Route path="/login" exact component={Login}/>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        user: state.userDetails.user
    }
}

export default connect(mapStateToProps)(withRouter(App));
