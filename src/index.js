
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from "react-redux";
import {reduxStore} from "./dux/AppStore";
import {MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
    <Provider
        store={reduxStore}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
        <BrowserRouter>

        <App />
        </BrowserRouter>
        </MuiPickersUtilsProvider>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
