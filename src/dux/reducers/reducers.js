import {FORM_FILLED, USER_LOGGED_IN, USER_LOGGED_OUT} from "../ActionTypes";

const initialState = {
    user:JSON.parse(localStorage.getItem('user'))
};

export const UserActionReducer=(state= initialState,action)=>{
    switch (action.type) {
        case USER_LOGGED_IN:
            return {
                ...state,
                user: action.user
            };
            // console.log(action)
            //  state={...state,user:action.user}
            //  return state
        case (USER_LOGGED_OUT):
            state.userDetails=undefined
            return {
                ...state,
                user:null
            }
        default:
            return state
    }
}
export const FormActionReducer=(state=initialState,action)=>{
    switch (action.type) {
        case FORM_FILLED:
            state.form=action.form
            return state
        default:
            return state
    }
}