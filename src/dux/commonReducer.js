import {combineReducers} from "redux";
import {FormActionReducer, UserActionReducer} from "./reducers/reducers";

export const commonReducer=combineReducers({userDetails:UserActionReducer,form:FormActionReducer})