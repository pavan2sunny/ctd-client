import {applyMiddleware, createStore} from "redux";
import {commonReducer} from "./commonReducer";
import thunk from "redux-thunk";

export const reduxStore=createStore(commonReducer,applyMiddleware(thunk))