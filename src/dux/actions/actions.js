import {USER_LOGGED_IN, USER_LOGGED_OUT} from "../ActionTypes";

export const userLoggedInAction=(state)=>{
    console.log(state)
    return {
        type:USER_LOGGED_IN,
        user:state
    }
}
export const userLoggedOutAction=(state)=>{
    return{
        type:USER_LOGGED_OUT,
        user:{}
    }
}